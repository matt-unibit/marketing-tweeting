import tweepy
import pandas as pd
import json
import requests
import os
import os.path
from datetime import datetime
import re
import getpass

if getpass.getuser() == 'Unibit':
    os.chdir('/Users/Unibit/Desktop/marketing-tweeting/src/')
if getpass.getuser() == 'data':
    os.chdir('/home/data/Updated_Files/src/marketing-tweeting/src')


def updateTwitterLog():
    #load or create dataframe
    if os.path.isfile('../data/tweetlog.csv'):
        tweets = pd.read_csv('../data/tweetlog.csv', encoding='utf-8')
    else:
        tweets = pd.DataFrame(columns=['source', 'title', 'publishAt', 'tweetStatus'])

    #scrape top 10 headlines from each source
    sources = ['bloomberg','financial-times','the-wall-street-journal','crypto-coins-news']
    for source in sources:
        r = requests.get('https://newsapi.org/v2/top-headlines?sources=' + source +
                         '&apiKey=4587b09332c44e479807fab40aea7ffa')
        dta = json.loads(r.text, encoding="utf-8")
        dta = dta['articles']
        for i in dta:
            if any(tweets['title'].isin([i['title']]).tolist()) or \
                    re.search("[,:?]", i['title']) or \
                    re.search("(Who|What|When|Why|Where|How|who|what|when|why| \
                                where|how|and|And|something|Something)", i['title']) or \
                    re.search("^(This|this|These|these|The|the|A guide|A Guide)", i['title']):
            # prevents duplicate tweets, removes non-sentences, removes questions, removes ambiguous tweets
                continue
            i['publishedAt'] = datetime.strptime(i['publishedAt'][0:19], '%Y-%m-%dT%H:%M:%S')
            d = {'source': [source], 'title': [i['title']], 'publishAt': [i['publishedAt']], 'tweetStatus': [0]}
            d = pd.DataFrame.from_dict(data=d)
            d = d[['source','title','publishAt','tweetStatus']]
            tweets = pd.concat([tweets, d], sort=True)

    #convert date, sort values, and only keep 200 most recent
    tweets['publishAt'] = pd.to_datetime(tweets['publishAt'])
    tweets = tweets.sort_values(by=['publishAt'], ascending=False)
    tweets = tweets.head(200)
    tweets.to_csv('../data/tweetlog.csv', encoding='utf-8', index=False)

def tweet():
    #grabs new headlines
    tweets = pd.read_csv('../data/tweetlog.csv', encoding='utf-8')

    #log onto twitter
    credentials = json.loads(open('../data/twitter_credentials.json').read())
    auth = tweepy.OAuthHandler(credentials['CONSUMER_KEY'], credentials['CONSUMER_SECRET'])
    auth.set_access_token(credentials['ACCESS_TOKEN'], credentials['ACCESS_SECRET'])
    api = tweepy.API(auth)

    #choose the headline to tweet
    for i in range(tweets.shape[0]):
        if tweets['tweetStatus'].iloc[i] == 0:
            try:
                s = re.sub("-", "", tweets['source'].iloc[i]) #makes for cleaner Hashtag
                api.update_status(tweets['title'].iloc[i] + " #" + s)
            except:
                continue
            tweets.at[i, 'tweetStatus'] = 1
            break

    tweets.to_csv('../data/tweetlog.csv', encoding='utf-8', index=False)

if __name__ == "__main__":
    updateTwitterLog()
    tweet()
