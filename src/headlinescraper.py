import json
import requests
import pandas as pd
import os.path
from datetime import datetime
import unicodedata

#used to convert a unicode string to datetime object
def convertdatetime(datestr):
    if type(datestr) is unicode:
        x = unicodedata.normalize('NFKD', datestr).encode('utf-8')
        x = datestr[0:19]
        x = datetime.strptime(x,'%Y-%m-%dT%H:%M:%S')
    else:
        x = datestr
    return x

def updateTwitterLog():
    #load or create dataframe
    if os.path.isfile('../data/tweetlog.csv'):
        tweets = pd.read_csv('../data/tweetlog.csv')
    else:
        tweets = pd.DataFrame(columns=['source', 'title', 'publishAt', 'tweetStatus'])


    #scrape top 10 headlines from each source
    sources = ['bloomberg','financial-times','the-wall-street-journal','crypto-coins-news']
    for source in sources:
        r = requests.get('https://newsapi.org/v2/top-headlines?sources=' + source + '&apiKey=4587b09332c44e479807fab40aea7ffa')
        dta = json.loads(r.text)
        dta = dta['articles']
        for i in dta:
            if any(tweets['title'].isin([i['title']]).tolist()):
                #prevents headlines from being added twice
                continue
            i['publishedAt'] = convertdatetime(i['publishedAt'])
            tweets.loc[0] = [source, i['title'], i['publishedAt'], 0]
            tweets.index = tweets.index + 1
            tweets = tweets.sort_index()

    #convert date, sort values, and keep 200 most recent
    tweets['publishAt'] = pd.to_datetime(tweets['publishAt'])
    tweets = tweets.sort_values(by=['publishAt'], ascending=False)
    tweets = tweets.head(200)
    tweets.to_csv('../data/tweetlog.csv', encoding='utf-8', index=False)


